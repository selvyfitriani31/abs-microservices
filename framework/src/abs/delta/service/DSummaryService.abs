delta DSummaryService;
uses MSummaryService;

adds import Summary, SummaryImpl from MSummaryModel;
adds import SummaryDb, SummaryDbImpl from MSummaryDbImpl;
adds import Income, IncomeImpl from MIncomeModel;
adds import IncomeDb, IncomeDbImpl from MIncomeDbImpl;
adds import IncomeService, IncomeServiceImpl from MIncomeService;
adds import IncomeResponse, IncomeResponseImpl from MIncomeResponse;
adds import Expense, ExpenseImpl from MExpenseModel;
adds import ExpenseDb, ExpenseDbImpl from MExpenseDbImpl;
adds import ExpenseService, ExpenseServiceImpl from MExpenseService;
adds import ExpenseResponse, ExpenseResponseImpl from MExpenseResponse;
adds import ABSHttpRequest from ABS.Framework.Http;
adds import Utility, UtilityImpl from ABS.Framework.Utility;
adds import ForeignUtility, ForeignUtilityImpl from ABS.Framework.ForeignUtility;

adds interface SummaryService {
    List<SummaryWithSaldo> list(ABSHttpRequest request);
}

adds class SummaryServiceImpl implements SummaryService {

    List<Income> getIncomes(ABSHttpRequest request) {
        IncomeDb orm = new local IncomeDbImpl();
        List<Income> incomes = orm.findAll("MIncomeModel.IncomeImpl_c");
        return incomes;
    }

    List<Expense> getExpenses(ABSHttpRequest request) {
        ExpenseDb expense_orm = new local ExpenseDbImpl();
        List<Expense> expenses = expense_orm.findAll("MExpenseModel.ExpenseImpl_c");
        return expenses;
    }

    List<SummaryWithSaldo> list(ABSHttpRequest request) {
        SummaryDb orm = new local SummaryDbImpl();
       
        List<SummaryWithSaldo> summariesWithSaldo = Nil;
        List<Income> allIncomes = this.getIncomes(request);
        List<Expense> allExpenses = this.getExpenses(request);
        IncomeService incomeService = new local IncomeServiceImpl();
        ExpenseService expenseService = new local ExpenseServiceImpl();

        Int count = 0;
        Int saldo = 0;

        while (count < length(allIncomes)) {
            SummaryWithSaldo summaryWithSaldo = new local SummaryWithSaldoImpl();
            Income thisLoopIncome = nth(allIncomes, count);
            IncomeResponse incomeResponse = incomeService.transformIntoIncomeResponse(thisLoopIncome);

            Int incomeId = incomeResponse.getId();
            summaryWithSaldo.setId(incomeId);

            String incomeProgramNames = incomeResponse.getProgramName();
            summaryWithSaldo.setProgramName(incomeProgramNames);

            String incomeDatestamp = incomeResponse.getDatestamp();
            summaryWithSaldo.setDatestamp(incomeDatestamp);

            String incomeDescription = incomeResponse.getDescription();
            summaryWithSaldo.setDescription(incomeDescription);

            Int incomeIncome = incomeResponse.getAmount();
            summaryWithSaldo.setIncome(incomeIncome);

            summaryWithSaldo.setExpense(0);

            saldo = saldo + incomeIncome;
            summaryWithSaldo.setSaldo(saldo);
            List<SummaryWithSaldo> newSummaries = appendright(summariesWithSaldo, summaryWithSaldo);
            summariesWithSaldo = newSummaries;
            count = count + 1;
        }

        count = 0;
        while (count < length(allExpenses)) {
            SummaryWithSaldo summaryWithSaldo = new local SummaryWithSaldoImpl();
            Expense thisLoopExpense = nth(allExpenses, count);
            ExpenseResponse expenseResponse = expenseService.transformIntoExpenseResponse(thisLoopExpense);

            Int expenseId = expenseResponse.getId();
            summaryWithSaldo.setId(expenseId);

            String expenseProgramNames = expenseResponse.getProgramName();
            summaryWithSaldo.setProgramName(expenseProgramNames);

            String expenseDatestamp = expenseResponse.getDatestamp();
            summaryWithSaldo.setDatestamp(expenseDatestamp);

            String expenseDescription = expenseResponse.getDescription();
            summaryWithSaldo.setDescription(expenseDescription);

            summaryWithSaldo.setIncome(0);

            Int expenseValue = expenseResponse.getAmount();
            summaryWithSaldo.setExpense(expenseValue);

            saldo = saldo - expenseValue;
            summaryWithSaldo.setSaldo(saldo);
            summariesWithSaldo = appendright(summariesWithSaldo, summaryWithSaldo);
            count = count + 1;
        }

        // sorting
        ForeignUtility foreignUtility = new local ForeignUtilityImpl();
        List<SummaryWithSaldo> sortedSummaries = foreignUtility.sort(summariesWithSaldo, "saldo");
        return sortedSummaries;
    }
}

modifies interface SummaryWithSaldo {
    removes String getDatestamp();
    adds Int getSaldo();
    adds Unit setSaldo(Int saldo);
}

modifies class SummaryWithSaldoImpl {
    adds [PK] Int id = 0;
    adds String description = "";
    adds Int income = 0;
    adds Int expense = 0;
    adds String programName = "";
    adds Int saldo = 0;

    adds Int getId() { return this.id; }
    adds Unit setId(Int id) { this.id = id; }
    adds String getProgramName() { return this.programName; }
    adds Unit setProgramName(String programName) { this.programName = programName; }
    adds Unit setDatestamp(String datestamp) { this.datestamp = datestamp; }
    adds String getDescription() { return this.description; }
    adds Unit setDescription(String description) { this.description = description; }
    adds Int getIncome() { return this.income; }
    adds Unit setIncome(Int income) { this.income = income; }
    adds Int getExpense() { return this.expense; }
    adds Unit setExpense(Int expense) { this.expense = expense; }
    adds Int getSaldo() { return this.saldo; }
    adds Unit setSaldo(Int saldo) { this.saldo = saldo + this.income - this.expense; }
}
