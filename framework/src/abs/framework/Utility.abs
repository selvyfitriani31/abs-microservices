module ABS.Framework.Utility;

export Utility, UtilityImpl;

interface Utility {
    Int stringToInteger(String s);
    Rat stringToRational(String s);
    Bool stringToBoolean(String s);
    List<String> splitString(String s, String separator);
    Int getIndex(String s, String ch);
    Int parseDigit(String ch);
    List<String> stringToListPair(String s);
    Pair<Int,Int> stringToPairInt(String s);
}

class UtilityImpl implements Utility {
    Int stringToInteger(String s) {
        String inputString = s;
        Int length = strlen(inputString);
        Int output = 0;

        if (length < 0) {
            throw PatternMatchFailException;
        }
        else {
            Bool negative = this.isNegativeNumber(inputString);

            if (negative) {
                inputString = substr(inputString, 1, length - 1);
            }

            length = strlen(inputString);
            Int idx = 0;
            Int tens = this.power(1, length);

            while (idx < length) {
                // Parse the first digit
                String ch = substr(inputString, 0, 1);
                Int digit = this.parseDigit(ch);

                // Multiply the digit to its "tens" values
                Int temp = digit * tens;

                // And add it to the output
                output = output + temp;

                // Move to the next digit
                idx = idx + 1;
                tens = tens / 10;

                // Reduce string to exclude parsed first digit
                Int remainingLength = strlen(inputString);
                inputString = substr(inputString, 1, remainingLength - 1);
            }

            if (negative) {
                output = output * -1;
            }
        }

        return output;
    }

    Rat stringToRational(String s) {
        String inputString = s;
        Int length = strlen(inputString);
        Rat output = 0;

        if (length < 1) {
            output = -1; // TODO: Should raise an exception
        }
        else {
            Bool negative = this.isNegativeNumber(inputString);

            if (negative) {
                inputString = substr(inputString, 1, length - 1);
            }

            length = strlen(inputString);
            Int idx = 0;
            Int tens = this.power(1, length);

            while (idx < length) {
                // Parse the first digit
                String ch = substr(inputString, 0, 1);
                Int digit = this.parseDigit(ch);

                // Multiply the digit to its "tens" values
                Int temp = digit * tens;

                // And add it to the output
                output = output + temp;

                // Move to the next digit
                idx = idx + 1;
                tens = tens / 10;

                // Reduce string to exclude parsed first digit
                Int remainingLength = strlen(inputString);
                inputString = substr(inputString, 1, remainingLength - 1);
            }

            if (negative) {
                output = output * -1;
            }
        }

        return output;
    }

    Bool stringToBoolean(String s) {
        return s == "True" || s == "true";
    }

    List<String> splitString(String s, String separator) {
        List<String> listWord = Nil;
        Int lenData = strlen(s);
        Int count = 0;
        Int last = 0;

        while(count < lenData) {
            String chr = substr(s,count,1);
            if(chr == separator) {
                Int startWord = 0;
                if (last > 0) {
                    startWord = last + 1;
                }
                Int lenWord = count - startWord;
                String word = substr(s,startWord,lenWord);
                listWord = appendright(listWord,word);
                last = count;
            }
            count = count +1;
            if (count == lenData) {
                Int startWord = 0;
                if (last > 0) {
                    startWord = last + 1;
                }
                Int lenWord = count - startWord;
                String word = substr(s,startWord,lenWord);
                listWord = appendright(listWord,word);
            }
        }
        return listWord;
    }

    Int power(Int a, Int b) {
        Int result = a;
        Int n = b;

        while (n > 1) {
            result = result * 10;
            n = n - 1;
        }

        return result;
    }

    Int parseDigit(String ch) {
        Int result = case ch {
            "0" => 0;
            "1" => 1;
            "2" => 2;
            "3" => 3;
            "4" => 4;
            "5" => 5;
            "6" => 6;
            "7" => 7;
            "8" => 8;
            "9" => 9;
            _ => -1;
        };

        if (result == -1) {
            throw PatternMatchFailException;
        }
        return result;
    }

    Bool isNegativeNumber(String s) {
        Int length = strlen(s);
        String firstDigit = substr(s, 0, 1);
        Bool result = False;

        return (firstDigit == "-");
    }

        /* 
        Method : getIndex
        Parameters : String string, String character
        Return value : boolean
        Purpose : to get index a character from a string
    */
    Int getIndex(String s, String ch){
        Int size = strlen(s);
        Int count = 0;
        Int index = -1;
        while (count < size){
            String chr = substr(s, count, 1);
            if (chr == ch) {
                index = count;
                size = 0; // to break
            }
            count = count + 1;
        }
        return index;
    }


    List<String> stringToListPair(String s) {
        List<String> result = Nil;
        String strNoWhiteSpace = this.deleteWhitespace(s);

        Int size = strlen(strNoWhiteSpace);
        
        String charFirst = substr(strNoWhiteSpace,0,1);
        String charLast = substr(strNoWhiteSpace,size-1,1);
        String fill = "";
        
        if (charFirst == "[" && charLast == "]") {
            fill = substr(strNoWhiteSpace,1,size-2);
        }

        String pairs = fill + ")"; // to manipulate
        String pair = "";
        
        Int fillSize = strlen(fill);
        Int count = 0;

        while (count < fillSize) {
            String char = substr(pairs,count,1);
            pair = pair + char;
            if (char == ")") {
                count = count + 1; // to skip "," after ")"
                result = appendright(result, pair);
                pair = "";
            }
            count = count + 1;
        }
        return result;
    }

    Pair<Int,Int> stringToPairInt(String s){
        Pair<Int, Int> result = Pair(0,0);

        Int size = strlen(s);
        String charFirst = substr(s,0,1);
        String charLast = substr(s, size-1,1);
        
        Int firstInt = 0;
        Int secondInt = 0;
        
        if (charFirst == "(" && charLast == ")") {
            String fill = substr(s,1,size-2);
            Int count = 0;
            String new_fill = this.deleteWhitespace(fill);
        
            // Split new_fill to get 2 integer
            Int indexComma = this.getIndex(new_fill, ",");
            List<String> splitFill = Nil;
            if (indexComma > 0){
                splitFill = this.splitString(new_fill, ",");
                Int sizeSplit = length(splitFill);
                if (sizeSplit == 2) {
                    String sFirstInt = nth(splitFill,0);
                    String sSecondInt = nth(splitFill, 1);
                    firstInt = this.stringToInteger(sFirstInt);
                    secondInt = this.stringToInteger(sSecondInt);  
              }
            }
        }
        result = Pair(firstInt, secondInt);
        return result;
    }

    String deleteWhitespace(String s) {
        Int size = strlen(s);
        Int count = 0;
        String result = "";
        while (count<size){
            String ch = substr(s, count, 1);
            if (ch != " " && ch != "\n" && ch != "\t") { 
                result = result + ch;
            }
            count = count + 1;
        }
        return result;   
    }
}
