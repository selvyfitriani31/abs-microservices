// fli code for ForeignUtility
package ABS.Framework.ForeignUtility;
import ABS.Framework.ForeignUtility.*;
import ABS.StdLib.appendright_f;
import ABS.StdLib.nth_f;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import MSummaryService.*;// for summary with saldo i
import abs.backend.java.lib.types.*; // for ABSSTring

class Pair<T,V>{
    
    private T key;
    private V value;

    public Pair(T key, V value){
        this.key = key;
        this.value = value;
    }

    public T getKey(){
        return this.key;
    }

    public V getValue(){
        return this.value;
    }

}

public class ForeignUtilityImpl_fli extends ForeignUtilityImpl_c {
    @Override
    public ABS.StdLib.List<SummaryWithSaldo_i> fli_sort(ABS.StdLib.List<SummaryWithSaldo_i> items, ABSString sortkey) {
        ArrayList<Pair<String, SummaryWithSaldo_i>> sorted_items = new ArrayList<Pair<String,SummaryWithSaldo_i>>();

        // put all item in "items" to an ArrayList of Pair(saldo, summaryObject)
        int ctr = 0;
        while(ctr < ABS.StdLib.length_f.apply(items).toInt()) {
        	ABSInteger abs_ctr = ABSInteger.fromInt(ctr);
            SummaryWithSaldo_i value = ABS.StdLib.nth_f.apply(items, abs_ctr);
        	String key = value.getDatestamp().toString();
            key = key.substring(1,key.length()-1);
            
        	Pair<String, SummaryWithSaldo_i> item = new Pair(key, value);
        	sorted_items.add(item);
            ctr++;
        }
        
        // sort
        sorted_items.sort(new Comparator<Pair<String, SummaryWithSaldo_i>>() {
            @Override
            public int compare(Pair<String, SummaryWithSaldo_i> o1, Pair<String, SummaryWithSaldo_i> o2) {
                SimpleDateFormat absDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                int compareResult = 0;
                try{
                    Date o1Date = absDateFormat.parse(o1.getKey());
                    Date o2Date = absDateFormat.parse(o2.getKey());
                    compareResult = o1Date.compareTo(o2Date);
                } catch (ParseException e){
                    System.out.println(e);
                }
                if (compareResult < 0) {
                    return -1;
                } else if (compareResult == 0) {
                    return 0; 
                } else {
                    return 1;
                }
            }
        });

        // reconstruct array
        ABS.StdLib.List<SummaryWithSaldo_i> result = new ABS.StdLib.List_Nil();
        for (Pair<String, SummaryWithSaldo_i> item : sorted_items) {
			result = appendright_f.apply(result, item.getValue());
		}
        
        // return
        return result;
    }    
    // todo bikin library auto reconstruct array
    // bikin auto convert abs list to array list
}
