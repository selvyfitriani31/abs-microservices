CREATE TABLE IF NOT EXISTS expense_audit (
	stamp			TIMESTAMP 	not null,
	userid 			TEXT		not null,
	datestamp       TEXT        not null,
	amount          INT        	not null,
	idProgram       INT        	not null,
	description     TEXT        not null,
	id            	INT			not null,	
	idCoa           INT        	not null
);

CREATE OR REPLACE FUNCTION process_expense_audit() RETURNS TRIGGER AS $expense_audit$	
    begin
		if (TG_OP = 'INSERT') then
			insert into expense_audit select now(), user, NEW.*;
		elsif (TG_OP = 'UPDATE') then
			insert into expense_audit select now(), user, OLD.datestamp, -OLD.amount, OLD.idProgram, OLD.description, OLD.id, OLD.idCoa;
			insert into expense_audit select now(), user, NEW.*;
			return NEW;
		elsif (TG_OP = 'DELETE') then
			insert into expense_audit select now(), user, OLD.*;
			return NEW;
		end if;
		return null;
	end;
$expense_audit$ LANGUAGE plpgsql;

CREATE TRIGGER expense_audit
AFTER INSERT OR UPDATE OR DELETE ON ExpenseImpl
	FOR EACH ROW EXECUTE PROCEDURE process_expense_audit();
