CREATE TABLE IF NOT EXISTS income_audit (
	stamp			TIMESTAMP 	not null,
	userid 			TEXT		not null,
	datestamp       TEXT        not null,
	amount          INT        	not null,
	idProgram       INT        	not null,
	description     TEXT        not null,
	paymentMethod   TEXT        		,
	id            	INT			not null,	
	idCoa           INT        	not null
);

CREATE OR REPLACE FUNCTION process_income_audit() RETURNS TRIGGER AS $income_audit$	
    begin
		if (TG_OP = 'INSERT') then
			insert into income_audit select now(), user, NEW.*;
		elsif (TG_OP = 'UPDATE') then
			insert into income_audit select now(), user, OLD.datestamp, -OLD.amount, OLD.idProgram, OLD.description, OLD.paymentMethod, OLD.id, OLD.idCoa;
			insert into income_audit select now(), user, NEW.*;
			return NEW;
		elsif (TG_OP = 'DELETE') then
			insert into income_audit select now(), user, OLD.*;
			return NEW;
		end if;
		return null;
	end;
$income_audit$ LANGUAGE plpgsql;

CREATE TRIGGER income_audit
AFTER INSERT OR UPDATE OR DELETE ON IncomeImpl
	FOR EACH ROW EXECUTE PROCEDURE process_income_audit();
