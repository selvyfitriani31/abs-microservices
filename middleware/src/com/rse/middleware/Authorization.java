/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rse.middleware;

import com.rse.middleware.oauth2.google.DefaultGoogleTokenVerifier;
import com.rse.middleware.oauth2.google.AlternativeGoogleTokenVerifier;
import com.rse.middleware.oauth2.facebook.DefaultFacebookTokenVerifier;
import com.rse.middleware.oauth2.facebook.AlternativeFacebookTokenVerifier;
import com.rse.middleware.oauth2.auth0.DefaultAuth0TokenVerifier;
import com.rse.middleware.oauth2.TokenVerifier;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import com.rse.middleware.oauth2.TokenPayload;

/**
 *
 * @author Afifun
 */
public class Authorization {

    public String type; // specify authentication type or provider
    private String[] roles;
    public HashMap<String, String> allowedEmail = new HashMap<>();
    private String clientId = null;
    private HashMap<String, TokenVerifier[]> strategies = new HashMap<>();

    public Authorization() {
        this.initializeStrategies();
        this.loadClientId();
    }

    public Authorization(String[] roles) {
        this();
        this.setRoles(roles);
    }

    public String getClientId() {
        return clientId;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
        this.loadAllowedEmail(roles);
    }

    private void setType(String type) {
        this.type = type.toLowerCase();
    }

    public Boolean authenticate(String token) throws Exception {
        if (token == null) {
            throw new TokenNullException("Token Is Needed");
        } else if (token.length() > 0) {
            TokenVerifier[] verifiers = this.strategies.getOrDefault(this.type, null);
            if (verifiers != null) {
                for (TokenVerifier verifier : verifiers) {
                    TokenPayload payload = verifier.verify(this.clientId, token);
                    if (payload == null) {
                        continue; // try another strategy
                    } else if (this.allowedEmail.get(payload.getEmail()) == null) {
                        // if email not allowed, reject
                        throw new NotPermittedException("You don't have permission to access this service");
                    } else {
                        return true;
                    }
                }
            } else {
                throw new TokenInvalidException("Invalid Authentication Type");
            }
        } else {
            // reject if token is empty
            throw new TokenEmptyException("Token Is Empty");
        }
        // if reached here, reject, because it's invalid (null for all strategies)
        throw new TokenInvalidException("Invalid Token");
    }

    private void initializeStrategies() {
        TokenVerifier[] googleStrategies = new TokenVerifier[2];
        googleStrategies[0] = new DefaultGoogleTokenVerifier();
        googleStrategies[1] = new AlternativeGoogleTokenVerifier();
        this.strategies.put("google", googleStrategies);
        TokenVerifier[] fbStrategies = new TokenVerifier[2];
        fbStrategies[0] = new DefaultFacebookTokenVerifier();
        fbStrategies[1] = new AlternativeFacebookTokenVerifier();
        this.strategies.put("facebook", fbStrategies);
        TokenVerifier[] auth0Strategies = new TokenVerifier[1];
        auth0Strategies[0] = new DefaultAuth0TokenVerifier();
        this.strategies.put("auth0", auth0Strategies);
    }

    private void loadClientId() {
        FileInputStream input = null;
        Properties prop = new Properties();

        try {
            input = new FileInputStream("auth.properties");
            // load a properties file
            prop.load(input);
            this.clientId = prop.getProperty("client_id");
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private void loadAllowedEmail(String[] roles) {
        FileInputStream input = null;
        Properties prop = new Properties();

        try {
            input = new FileInputStream("auth.properties");
            // load a properties file
            prop.load(input);

            this.setType(prop.getProperty("auth").trim());
            String administrator_email = prop.getProperty("administrator");
            this.allowedEmail = new HashMap<>();
            this.allowedEmail.put(administrator_email.trim(), administrator_email.trim());

            for (String role : roles) {
                String[] temp = prop.getProperty(role).split(",");
                for (String email : temp) {
                    this.allowedEmail.put(email.trim(), email.trim());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }
    }
}

class TokenNullException extends Exception {

    public TokenNullException() {
    }

    public TokenNullException(String message) {
        super(message);
    }
}

class TokenEmptyException extends Exception {

    public TokenEmptyException() {
    }

    public TokenEmptyException(String message) {
        super(message);
    }
}

class TokenInvalidException extends Exception {

    public TokenInvalidException() {
    }

    public TokenInvalidException(String message) {
        super(message);
    }
}

class NotPermittedException extends Exception {

    public NotPermittedException() {
    }

    public NotPermittedException(String message) {
        super(message);
    }
}
